# Flectra Community / credit-control

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_invoice_overdue_warn](account_invoice_overdue_warn/) | 2.0.1.0.0| Show warning on customer form view if it has overdue invoices
[sale_financial_risk](sale_financial_risk/) | 2.0.1.3.1| Manage partner risk in sales orders
[account_credit_control](account_credit_control/) | 2.0.1.5.0| Account Credit Control
[account_payment_return_financial_risk](account_payment_return_financial_risk/) | 2.0.1.0.0| Partner Payment Return Risk
[sale_financial_risk_info](sale_financial_risk_info/) | 2.0.1.1.0| Adds risk consumption info in sales orders.
[account_financial_risk](account_financial_risk/) | 2.0.2.2.1| Manage customer risk
[partner_risk_insurance](partner_risk_insurance/) | 2.0.1.1.0| Risk insurance partner information
[account_invoice_overdue_warn_sale](account_invoice_overdue_warn_sale/) | 2.0.1.0.0| Show overdue warning on sale order form view
[account_invoice_overdue_reminder](account_invoice_overdue_reminder/) | 2.0.1.5.0| Simple mail/letter/phone overdue customer invoice reminder 
[stock_financial_risk](stock_financial_risk/) | 2.0.1.0.1| Manage partner risk in stock moves


