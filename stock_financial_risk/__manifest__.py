# Copyright 2016 Carlos Dauden <carlos.dauden@tecnativa.com>
# Copyright 2017 David Vidal <david.vidal@tecnativa.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Partner Stock Risk",
    "summary": "Manage partner risk in stock moves",
    "version": "2.0.1.0.1",
    "category": "Sales Management",
    "license": "AGPL-3",
    "author": "Tecnativa, " "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/credit-control",
    "depends": ["stock", "account_financial_risk"],
    "installable": True,
}
