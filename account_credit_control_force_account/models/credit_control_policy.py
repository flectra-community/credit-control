from flectra import api, fields, models, _

class CreditControlPolicy(models.Model):

    _inherit = "credit.control.policy"

    def _move_lines_subset(self, credit_control_run, model, move_relation_field):
        to_add, to_remove = super(CreditControlPolicy, self)._move_lines_subset(credit_control_run, model, move_relation_field)

        default_domain = self._move_lines_domain(credit_control_run)
        domain = list(default_domain)
        domain.append((move_relation_field, "not in", (to_add | to_remove).ids))
        to_add |= to_add.search(domain)

        return to_add, to_remove
