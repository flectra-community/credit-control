====================================
Account Credit Control Force Account
====================================

Check Policy also when it is not set on invoice.


**Table of contents**

.. contents::
   :local:

Configuration
=============

No configuration needed. Only install the module.

Usage
=====

The main module does search for credit lines always with the policy set on the invoice.
This module will change this behaviour to also search for credit lines when the policy
is not set on the invoice. Then it will check the account in relation to the account set on policy.


Authors
~~~~~~~

* 2BIT -> Jamotion

Contributors
~~~~~~~~~~~~

* Renzo Meister (Jamotion GmbH9
