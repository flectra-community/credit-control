# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Account Credit Control Force Acount",
    "version": "2.0.1.2.0",
    "author": "2BIT -> Jamotion",
    "maintainer": "2BIT -> Jamotion",
    "category": "Finance",
    "depends": ["account_credit_control"],
    "website": "https://jamotion.ch",
    "data": [],
    "installable": True,
    "license": "AGPL-3",
    "application": False,
}
